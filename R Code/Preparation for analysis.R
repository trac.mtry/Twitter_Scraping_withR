##We are going to work with the same two dataframe that we downloaded last video


DT  ##list downloaded of Director_TRAC tweets
DTdf  #dataFrame of those tweets

tweets2  ##list of tweets with the word 'starrealms' in the tweet
tweets2DF  ##dataframe of those tweets


##new packages we will need:
library("dplyr")
library("tidyr")
library("tm")

##starting with the DTdf dataframe

##number of tweets per day/month
PB_Daily_Tweets <- DTdf %>%
  group_by(cut(as.Date(created), "day")) %>%
  summarise(Count = n())

PB_Daily_Tweets

mean(PB_Daily_Tweets$Count)

PB_Weekly_Tweets <- DTdf %>%
  group_by(cut(as.Date(created), "week")) %>%
  summarise(Count = n())

PB_Weekly_Tweets

mean(PB_Weekly_Tweets$Count)

mean(DTdf$retweetCount)

DT<-userTimeline(user = username, n=50, includeRts=TRUE)  #If you want to include retweets

##what about the number and names of her followers?
user1<-getUser(username)
PBfollowerCount<-user1$getFollowersCount()
PBfollowerCount

PBfollowers<-user1$getFollowerIDs()
PBfollowers

user2<-getUser(PBfollowers[1])

########What about the data using the key word search?
##Is StarRealms a popular topic?
tweets2DF$created

SR_Hourly_Tweets <- tweets2DF %>%
  group_by(cut(as.Date(created), "days")) %>%
  summarise(Count = n())

mean(SR_Hourly_Tweets$Count)


keyWord<-"GEN Nicholson"  ##lets compare starrealms to something most likely more popular

tweets2<-searchTwitter(searchString=keyWord, n=1000)
tweets2DF<-twListToDF(tweets2)

SR_Hourly_Tweets <- tweets2DF %>%
  group_by(cut(as.Date(created), "days")) %>%
  summarise(Count = n())

SR_Hourly_Tweets

mean(SR_Hourly_Tweets$Count)
